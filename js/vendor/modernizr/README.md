#Modernizr 3.3.1 (Custom Build) | MIT

This build is created with Drupal 8 standards in mind; IE9 as the minimum
requirement. The current tests are meant for those who would like to test
some newer CSS properties while still providing a fallback.

##Features
* classlist
* cssanimations
* csscolumns
* csshairline
* csspositionsticky
* csstransforms3d
* flexbox
* flexboxlegacy
* flexboxtweener
* flexwrap
* localstorage
* scrollsnappoints
* sessionstorage
* shapes
* svgclippaths
* svgfilters
* webworkers
* wrapflow
* setclasses

[Custom build](https://modernizr.com/download/?-classlist-cssanimations-csscolumns-csshairline-csspositionsticky-csstransforms3d-flexbox-flexboxlegacy-flexboxtweener-flexwrap-localstorage-scrollsnappoints-sessionstorage-shapes-svgclippaths-svgfilters-webworkers-wrapflow-setclasses)
