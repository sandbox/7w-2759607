/**
 * @file
 * Defines the behavior of the theme scripts
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  /**
   * Container for easy storage and retrieval of variables in the DOM
   *
   * Configurable Javascript is available with drupalSettings.
   * However, to make drupalSettings available to our JavaScript file: we have to declare a dependency on it.
   *
   * @requires drupalSettings as dependency THEMENAME.libraries.yml
   * @see https://www.drupal.org/node/2274843#configurable
   * @type {{object}}
   */
  drupalSettings.THEMENAME = {
    'config' : {
        'example_variable': true
    }
  };

  /**
   * Sample of Drupal.behaviors
   *
   * @see https://www.drupal.org/node/2269515
   * @type {{attach: Drupal.behaviors.myCustomThemeBehavior.attach}}
   */
  Drupal.behaviors.myCustomThemeBehavior = {
    attach: function (context, settings) {
      $(context).find('.css-class').once('myCustomThemeBehavior').each(function () {
          // Apply the myCustomThemeBehavior effect to the elements only once.
      });
    }
  };

}(jQuery, Drupal, drupalSettings));
