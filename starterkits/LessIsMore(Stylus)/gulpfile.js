'use strict';





//////////////////////////////////
//
// Gulp settings
//
// - Settings
// - Main tasks
// - Gulp tasks
//
//////////////////////////////
/* Init Gulp */
var gulp = require('gulp');
/* Init Gulp plugins */
var plugins = require('gulp-load-plugins')();
/* Set paths  */
var _p = {
  styles: './styles',
  stylesComp: './css',
  img: './images',
  imgMin: './images_min',
  layouts: './layouts',
};
/* CSS settings */
var _s = {
  prefixVersion: 'last 2 versions',
  cssCompatibility:  'ie10',
  cssCompress: true,
  cssMinify: true,
  useBEM: false,
};





//////////////////////////////////
//
// Main Gulp tasks
// - Prefix and minimize files
// - critical.css and main.css is a combination of all the CSS fiels
//   that are set using the index.styl and critical.styl files
// - bundles are generater from sources/bundles folder
// - layouts are generated into their own folders
//
//////////////////////////////
/* Main CSS files */
gulp.task('css', function () {
  gulp.src([_p.styles + '/sources/*.styl'], { base: _p.stylesComp })
    .pipe(plugins.stylus({
        paths: [_p.styles + '/settings'],
        import: ['mixins', 'placeholders'],
        compress: _s.cssCompress
      }))
    .pipe(plugins.autoprefixer({
        browsers: [_s.prefixVersion],
      }))
    .pipe(plugins.cleanCss({
      compatibility: _s.cssCompatibility,
      semanticmerging: _s.useBEM
    }))
    .pipe(gulp.dest(_p.stylesComp))
});
/* Layout files compiled */
gulp.task('layouts', function () {
  gulp.src([_p.layouts + '/**/*.styl'])
    .pipe(plugins.stylus({
        paths: [_p.styles + '/settings'],
        import: ['mixins', 'placeholders'],
        compress: _s.cssCompress
      }))
    .pipe(plugins.autoprefixer({
        browsers: [_s.prefixVersion],
      }))
    .pipe(plugins.cleanCss({
      compatibility: _s.cssCompatibility,
      semanticmerging: _s.useBEM
    }))
    .pipe(gulp.dest(_p.layouts))
});



/*
 * Minimize images
 * - consider using `pngquant` if you are using PNG -files
 */
 gulp.task('imagemin', function () {
   return gulp.src([_p.img, _p.img + '/**/*'])
       .pipe(plugins.imagemin({
         progressive: true
       }))
       .pipe(gulp.dest(_p.imgMin));
 });





//////////////////////////////////
//
// Gulp tasks
//
//////////////////////////////
/* Watch for CSS changes */
gulp.task('styles', function () {
   gulp.watch([
               _p.styles + '/*.styl',
               _p.styles + '/**/*.styl'
             ], ['css', 'layouts']);
});



/* Initiate default tasks */
gulp.task('default', [
                      'css',
                      'layouts',
                      'styles'
                     ]);



/* FIN */
